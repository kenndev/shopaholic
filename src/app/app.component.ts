import {Component} from '@angular/core';
import {AlertController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {SqlitedataProvider} from "../providers/sqlitedata/sqlitedata";
import {TabsPage} from '../pages/tabs/tabs';
import {Network} from '@ionic-native/network';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;


  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              alertCtrl: AlertController,
              network: Network,
              sqliteDataProvider: SqlitedataProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      splashScreen.hide();

      //Check for internet connectivity
      network.onDisconnect().subscribe(data => {
        let confirm = alertCtrl.create({
          title: 'Alert!',
          message: 'Woops!!! You do not have internet access. Please make sure you have a working internet connection to proceed',
          buttons: [
            {
              text: 'close',
              handler: () => {
              }
            }
          ]
        });
        confirm.present();
      }, error => console.error(error));

      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString('#af1e00');
      //Check if sqlite databases needed are available if not create them
      sqliteDataProvider.createDatabase();
      sqliteDataProvider.createWishList();
      sqliteDataProvider.countCartLabelTabs();
      sqliteDataProvider.countWishListLabelTabs();
      sqliteDataProvider.createSearchHistory();
      sqliteDataProvider.createShippingAddress();
      sqliteDataProvider.createBillingAddress();
      sqliteDataProvider.createProfile();

    });
  }
}
