import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {WishlistPage} from '../pages/wishlist/wishlist';
import {CartPage} from '../pages/cart/cart';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';
import {OrdersPage} from '../pages/orders/orders';
import {CategoryPage} from "../pages/category/category";
import {FilterPage} from "../pages/filter/filter";
import {ProductDetailsPage} from "../pages/product-details/product-details";
import {ProductsPage} from "../pages/products/products";
import {CheckoutPage} from "../pages/checkout/checkout";
import {LoginPage} from "../pages/login/login";
import {SignupPage} from "../pages/signup/signup";
import {ResetPasswordPage} from "../pages/reset-password/reset-password";
import {ProfilePage} from "../pages/profile/profile";
import {OrderDetailsItemPage} from "../pages/order-details-item/order-details-item";

import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {FIREBASE_CONFIG} from './app.firebase.config';
import {PayPal} from "@ionic-native/paypal";
import {Stripe} from '@ionic-native/stripe';
import {EmailComposer} from '@ionic-native/email-composer';
import {FirebaseProvider} from '../providers/firebase/firebase';
import {RlTagInputModule} from 'angular2-tag-input';
import {IonicImageLoader} from 'ionic-image-loader';
import {ProductsPipe} from "../pipes/products/products";
import {FilterProductsPipe} from "../pipes/filter-products/filter-products";
import {IonicStorageModule} from "@ionic/storage";
import {SQLite} from "@ionic-native/sqlite";
import {StatusBar} from '@ionic-native/status-bar';
import {Keyboard} from '@ionic-native/keyboard';
import {SplashScreen} from '@ionic-native/splash-screen';
import {HttpModule} from '@angular/http';
import {Network} from '@ionic-native/network';
import {Camera} from '@ionic-native/camera';
import { FileTransfer} from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

import {SqlitedataProvider} from '../providers/sqlitedata/sqlitedata';
import {DataProvider} from '../providers/data/data';
import {CameraProvider} from '../providers/camera/camera';


@NgModule({
  declarations: [
    MyApp,
    WishlistPage,
    CartPage,
    HomePage,
    TabsPage,
    OrdersPage,
    CategoryPage,
    ProductsPage,
    FilterPage,
    OrderDetailsItemPage,
    CheckoutPage,
    ProductDetailsPage,
    LoginPage,
    SignupPage,
    ProfilePage,
    ResetPasswordPage,
    ProductsPipe,
    FilterProductsPipe,

  ],
  imports: [
    BrowserModule,
    RlTagInputModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      scrollAssist: false,
      autoFocusAssist: false
    }),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    IonicImageLoader.forRoot(),
    IonicStorageModule.forRoot(),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WishlistPage,
    CartPage,
    HomePage,
    TabsPage,
    OrdersPage,
    CategoryPage,
    ProductsPage,
    CheckoutPage,
    FilterPage,
    LoginPage,
    SignupPage,
    ProfilePage,
    ResetPasswordPage,
    ProductDetailsPage,
    OrderDetailsItemPage
  ],
  providers: [
    StatusBar,
    Keyboard,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    SqlitedataProvider,
    SQLite,
    FirebaseProvider,
    PayPal,
    Stripe,
    EmailComposer,
    Network,
    Camera,
    CameraProvider,FileTransfer,
    File
  ]
})
export class AppModule {
}
