import {Pipe, PipeTransform} from '@angular/core';
import {DataProvider} from "../../providers/data/data";

/**
 * Generated class for the ProductsPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'products',
})
export class ProductsPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */

  public data: any;
  public errorMessage: string;
  public productsArray: any[] = [];
  public finalArr: any[];

  constructor(public dataprovider: DataProvider) {
  }

  transform(obj: any, searchTerm: string): any[] {

    // return list.filter(function (products) {
    //   console.log(products['categoryId'] == searchTerm);
    //   return products['categoryId'] == searchTerm;
    // });
    //console.log(this.productsArray);
    this.getCategoriesProducts(obj['categoryId']);
    return this.productsArray;


  }

  getCategoriesProducts(id): any[] {

    this.dataprovider.getProductsByCategory(id)
      .subscribe(
        res => {
          this.data = res;
          for (let i = 0; i < this.data.products.length; i++) {
            this.productsArray.push(this.data.products[i]);
          }
          //this.productsArray.push(this.data.products);
          return this.productsArray;
          //this.finalArr = this.productsArray;
        },
        error => this.errorMessage = <any>error);
    return this.productsArray;
  }

}
