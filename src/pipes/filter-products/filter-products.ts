import {Pipe, PipeTransform} from '@angular/core';

/**
 * Generated class for the FilterProductsPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filterproducts',
})
export class FilterProductsPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(list: any[], searchTerm: any[]): any[]{

    //console.log(searchTerm['categoryId']);

    return list.filter(function (products) {
      console.log(products['categoryId'] == searchTerm['categoryId']);
      return products['categoryId'] == searchTerm['categoryId'];
    });

  }
}
