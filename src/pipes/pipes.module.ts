import { NgModule } from '@angular/core';
import { ProductsPipe } from './products/products';
import {DataProvider} from "../providers/data/data";
import { FilterProductsPipe } from './filter-products/filter-products';

@NgModule({
	declarations: [ProductsPipe,
    FilterProductsPipe],
	imports: [DataProvider],
	exports: [ProductsPipe,
    FilterProductsPipe]
})
export class PipesModule {}
