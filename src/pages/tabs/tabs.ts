import {Component} from '@angular/core';

import {WishlistPage} from '../wishlist/wishlist';
import {CartPage} from '../cart/cart';
import {HomePage} from '../home/home';
import {OrdersPage} from '../orders/orders';
import {Events} from 'ionic-angular';
import {ProfilePage} from "../profile/profile";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = WishlistPage;
  tab3Root = CartPage;
  tab5Root = ProfilePage;
  cartItemsNumber: String;
  wishListNumber: String;

  constructor(public events: Events) {

  }

  ionViewDidEnter() {
    this.events.subscribe('countCart', (cart) => {


      this.cartItemsNumber = cart;
    });
    this.events.subscribe('countWishList', (wishlist) => {


      this.wishListNumber = wishlist;
    });
  }
}
