import {Component} from '@angular/core';
import {
  AlertController, IonicPage, ModalController, NavController, NavParams, ToastController, ActionSheetController,
  Platform, LoadingController
} from 'ionic-angular';
import {FirebaseProvider} from "../../providers/firebase/firebase";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {HomePage} from "../home/home";
import {AngularFireAuth} from "angularfire2/auth";
import {LoginPage} from "../login/login";
import {OrdersPage} from "../orders/orders";
import {EmailComposer} from '@ionic-native/email-composer';
import {CameraProvider} from "../../providers/camera/camera";
import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import {DataProvider} from "../../providers/data/data";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  logedin: boolean;
  user: any;
  data: any;
  errorMessage: any;
  profile: any = [];
  placeholder = 'assets/imgs/profile/profile.jpg';
  chosenPicture: any;

  constructor(public navCtrl: NavController,
              public fireBaseProvider: FirebaseProvider,
              private afAuth: AngularFireAuth,
              private sqliteDataProvider: SqlitedataProvider,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              private toastCtrl: ToastController,
              public actionsheetCtrl: ActionSheetController,
              public platform: Platform,
              private dataProvider: DataProvider,
              private cameraProvider: CameraProvider,
              public emailComposer: EmailComposer,
              public loadingCtrl: LoadingController,
              public navParams: NavParams) {

  }

  ionViewDidEnter() {
    this.getUserSqlite();
    this.user = this.afAuth.auth.currentUser;
  }



  logOut() {
    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'You are about to logout. Click yes to continue and no to cancel.',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.fireBaseProvider.logoutUser().then(() => {
              this.chosenPicture = 'assets/imgs/profile/profile.jpg';
              this.sqliteDataProvider.deleteShipping();
              this.sqliteDataProvider.deleteBilling();
              this.sqliteDataProvider.deleteProfile();
              this.presentToast("Bye bye. Log out successful");
              this.navCtrl.parent.select(0, HomePage);
            });
          }
        }
      ]
    });
    confirm.present();
  }

  login() {
    let profileModal = this.modalCtrl.create(LoginPage, {isModal: true});
    profileModal.onDidDismiss(() => {
      this.getUserSqlite();
    });
    profileModal.present();
  }

  chat() {
    this.navCtrl.push("ChatPage");
  }

  orders() {
    this.navCtrl.push(OrdersPage);
  }

  emails() {
    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {
        //Now we know we can send

      }
    });

    let email = {
      to: 'info@allthingzcode.com',
      subject: 'Shopaholic',
      isHtml: true
    };

    this.emailComposer.open(email);
  }

  addresses() {
    this.navCtrl.push('AddressesPage');
  }

  shippingAddress() {
    this.navCtrl.push('ShippingPageListPage');
  }

  billingAddress() {
    this.navCtrl.push('BillingPageListPage');
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  changePicture() {

    if (this.profile.length>0) {
      const actionsheet = this.actionsheetCtrl.create({
        title: 'upload picture',
        buttons: [
          {
            text: 'camera',
            icon: !this.platform.is('ios') ? 'camera' : null,
            handler: () => {
              this.takePicture();
            }
          },
          {
            text: !this.platform.is('ios') ? 'gallery' : 'camera roll',
            icon: !this.platform.is('ios') ? 'image' : null,
            handler: () => {
              this.getPicture();
            }
          },
          {
            text: 'cancel',
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'destructive',
            handler: () => {
              console.log('the user has cancelled the interaction.');
            }
          }
        ]
      });
      return actionsheet.present();
    } else {
      let confirm = this.alertCtrl.create({
        title: 'Alert!',
        message: 'Please login to continue',
        buttons: [
          {
            text: 'No',
            handler: () => {
              this.navCtrl.pop();
            }
          },
          {
            text: 'Yes',
            handler: () => {
              let profileModal = this.modalCtrl.create(LoginPage, {isModal: true});
              profileModal.present();
            }
          }
        ]
      });
      confirm.present();
    }


  }

  takePicture() {
    const loading = this.loadingCtrl.create();
    loading.present();
    return this.cameraProvider.getPictureFromCamera().then(picture => {
      if (picture) {

        this.chosenPicture = picture;
      }

      loading.dismiss();
    }, error => {
      alert(error);
    });
  }

  getPicture() {
    const loading = this.loadingCtrl.create();

    loading.present();
    return this.cameraProvider.getPictureFromPhotoLibrary().then(picture => {
      if (picture) {
        this.chosenPicture = picture;
      }
      loading.dismiss();
    }, error => {
      alert(error);
    });
  }

  getProfile(uid) {
    this.dataProvider.getProfile(uid)
      .subscribe(
        res => {
          this.data = res;
          this.chosenPicture = this.data.profile.image;
        },
        error => this.errorMessage = <any>error);
  }

  getUserSqlite() {
    this.sqliteDataProvider.loadProfile().then(user => {
      this.profile = user;
      this.getProfile(this.profile[0]["uid"]);
    });
  }

}
