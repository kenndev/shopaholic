import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";

/**
 * Generated class for the OrderDetailsItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-details-item',
  templateUrl: 'order-details-item.html',
})
export class OrderDetailsItemPage {
  product_id: number;
  productName: string;
  productDescription: string;
  categoryName: string;
  old_price: string;
  price: string;
  image: string;
  categoryId: string;
  data: any;
  imageArray: any = [];
  errorMessage: string;
  hide: boolean = false;
  color: string;
  size: string;
  rate: number;
  currency_symbol: string;

  constructor(public navCtrl: NavController,
              public dataprovider: DataProvider,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.product_id = this.navParams.get('product_id');
    this.productName = this.navParams.get('productName');
    this.productDescription = this.navParams.get('productDescription');
    this.old_price = this.navParams.get('old_price');
    this.price = this.navParams.get('price');
    this.categoryId = this.navParams.get('categoryId');
    this.categoryName = this.navParams.get('categoryName');
    this.image = this.navParams.get('image_url');
    this.getProductsImages(this.product_id);
    this.rate = this.navParams.get('rate');
    this.currency_symbol = this.navParams.get('currency_symbol');
  }

  getProductsImages(id) {
    this.dataprovider.getProductsImages(id)
      .subscribe(
        res => {
          this.data = res;
          this.imageArray = this.data.images;
        },
        error => this.errorMessage = <any>error);
  }

  printRating(rating) {

    let max_rate = 5;
    let rounded_rating = Math.round(rating);
    let array_stars = new Array(max_rate);
    array_stars.fill('star-outline');

    for (let i = 0; i < rounded_rating; i++) {
      array_stars[i] = 'star';

      if (i === rounded_rating - 1 && rating % 1 !== 0) {
        array_stars[i] = 'star-half';
      }
    }

    return array_stars;
  }

}
