import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderDetailsItemPage } from './order-details-item';

@NgModule({
  declarations: [
    OrderDetailsItemPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderDetailsItemPage),
  ],
})
export class OrderDetailsItemPageModule {}
