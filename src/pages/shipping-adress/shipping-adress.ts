import {Component} from '@angular/core';
import {Events, IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {EmailValidator} from "../../validators/email";
import {AngularFireAuth} from "angularfire2/auth";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";

/**
 * Generated class for the ShippingAdressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shipping-adress',
  templateUrl: 'shipping-adress.html',
})
export class ShippingAdressPage {
  data: any;
  user: any;
  errorMessage: string;
  countries: any = [];
  billingAddressArray: any = [];
  public shippingForm: FormGroup;
  address = {
    radiobuttons: ''
  };
  displayShippingForm: boolean = false;
  radioButtonsDisplay: boolean;

  constructor(public navCtrl: NavController,
              public dataProvider: DataProvider,
              private toastCtrl: ToastController,
              private afAuth: AngularFireAuth,
              public formBuilder: FormBuilder,
              public sqliteDataProvider: SqlitedataProvider,
              public events: Events,
              public loadingCtrl: LoadingController,
              public navParams: NavParams) {
    this.navParams.get('first_name');
    this.shippingForm = formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone_number: ['', Validators.required],
      post_code: ['',],
      state: ['', Validators.required],
      town: ['', Validators.required],
      country: [''],
      email: ['',
        Validators.compose([Validators.required, EmailValidator.isValid])],
      address: [''],
    });
  }

  ionViewDidEnter() {
    this.getCountries();
    this.getBillingAddress();
  }

  getBillingAddress() {
    this.sqliteDataProvider.loadBillingAddress().then((billingAddress) => {
      this.billingAddressArray = billingAddress;
      if (this.billingAddressArray.length > 0) {
        this.radioButtonsDisplay = true;
      } else {
        this.radioButtonsDisplay = false;
        this.displayShippingForm = true;
      }
    });
  }

  getCountries() {
    this.dataProvider.getCountries()
      .subscribe(
        res => {
          let data = res;
          this.countries = data;

        },
        error => this.errorMessage = <any>error);
  }

  addShippingForm() {

    this.user = this.afAuth.auth.currentUser;

    let shippingData = {
      first_name: this.shippingForm.value.first_name,
      last_name: this.shippingForm.value.last_name,
      phone_number: this.shippingForm.value.phone_number,
      post_code: this.shippingForm.value.post_code,
      state: this.shippingForm.value.state,
      town: this.shippingForm.value.town,
      country: this.shippingForm.value.country,
      email: this.shippingForm.value.email,
      address: this.shippingForm.value.address,
      firebase_id: this.user.uid,

    }
    let loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    loading.present();
    this.dataProvider.saveShippingAddress(shippingData).subscribe(res => {
      this.data = res;
      if (this.data.result == 'success') {
        loading.dismiss();
        this.presentToast(this.data.message);
        this.navCtrl.pop();
      } else {
        loading.dismiss();
        this.presentToast(this.data.message);
      }

    }, error => this.errorMessage = <any>error);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  sameAsBillingAddress() {

    if (this.address.radiobuttons == 'yes') {
      let user = this.afAuth.auth.currentUser;
      this.displayShippingForm = false;

      let shippingData = {
        address_id: this.billingAddressArray[0]['address_id'],
        first_name: this.billingAddressArray[0]['first_name'],
        last_name: this.billingAddressArray[0]['last_name'],
        phone_number: this.billingAddressArray[0]['phone_number'],
        country: this.billingAddressArray[0]['country'],
        state: this.billingAddressArray[0]['state'],
        town: this.billingAddressArray[0]['town'],
        address: this.billingAddressArray[0]['address'],
        email: this.billingAddressArray[0]['email'],
        firebase_id: user.uid,

      };
      let loading = this.loadingCtrl.create({
        spinner: 'dots'
      });
      loading.present();
      this.dataProvider.saveShippingAddress(shippingData).subscribe(res => {
        this.data = res;
        if (this.data.result == 'success') {
          loading.dismiss();
          this.presentToast(this.data.message);
          this.sqliteDataProvider.addShipping(shippingData);

          this.events.publish('shipping:added');
          this.navCtrl.pop();
        } else {
          loading.dismiss();
          this.presentToast(this.data.message);
        }

      }, error => this.errorMessage = <any>error);

    } else {
      this.displayShippingForm = true;
    }
  }

}
