import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingAdressPage } from './shipping-adress';

@NgModule({
  declarations: [
    ShippingAdressPage,
  ],
  imports: [
    IonicPageModule.forChild(ShippingAdressPage),
  ],
})
export class ShippingAdressPageModule {}
