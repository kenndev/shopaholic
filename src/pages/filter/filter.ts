import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {ProductsPage} from "../products/products";
import {ProductDetailsPage} from "../product-details/product-details";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  data: any;
  errorMessage: string;
  categoryArray: any = [];
  open: boolean;
  products: any = [];
  name: string;
  productsFilteredArray: any = [];
  searchFilteredArray: any = [];
  searchData: any = [];
  categoryProductCountArray: any = [];
  filteredCategoryProductCountArray: any = [];
  settings: any = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public SqlitedataProvider: SqlitedataProvider,
              public dataprovider: DataProvider) {

  }

  ionViewDidEnter() {
    this.getCategories();
    this.loadSearchData();

  }

  getCategories() {

    this.dataprovider.getCategoriesAll()
      .subscribe(
        res => {

          this.data = res;
          this.categoryArray = this.data.category;
          this.categoryProductCountArray = this.data.product_count;
          this.settings = {
            currency_symbol: this.data.settings.currency_symbol
          }

        },
        error => this.errorMessage = <any>error);
  }

  numberOfProductsInCategory(categoryId) {
    this.filteredCategoryProductCountArray = this.categoryProductCountArray.filter(function (items) {
      console.log(items['categoryId'] == categoryId);
      return items['categoryId'] == categoryId;
    });

    return this.filteredCategoryProductCountArray[0]['count'];

  }

  toggleSection(i) {
    this.open = !this.open;
  }


  listProducts(id, catName) {
    this.navCtrl.push(ProductsPage, {categoryId: id, categoryName: catName});
  }

  onInput(env: any) {
    let searchValue = env.target.value;
    if (searchValue.trim() == "") {
      this.products.length = 0;
      return this.products;
    } else {
      this.dataprovider.search(searchValue)
        .subscribe(
          res => {
            console.log(searchValue);
            this.data = res;
            this.products = this.data.products;
            console.log(this.products);
            return this.products.filter(item => item.productName.toLowerCase().startsWith(searchValue.toLowerCase()))


          },
          error => this.errorMessage = <any>error);
    }
  }

  onCancel(env: any) {

  }

  productDetails(productId) {
    this.productsFilteredArray = this.products.filter(function (products) {
      console.log(products['productId'] == productId);
      return products['productId'] == productId;
    });

    let params = {
      'product_id': productId,
      'productName': this.productsFilteredArray[0].productName,
      'productDescription': this.productsFilteredArray[0].productDescription,
      'old_price': this.productsFilteredArray[0].old_price,
      'price': this.productsFilteredArray[0].price,
      'categoryId': this.productsFilteredArray[0].categoryId,
      'categoryName': this.productsFilteredArray[0].category_name,
      'color': this.productsFilteredArray[0].color,
      'size': this.productsFilteredArray[0].size,
      'image_url': this.productsFilteredArray[0].image,
      'rate': this.productsFilteredArray[0].rate,
      'currency_symbol': this.settings.currency_symbol,
    };

    this.SqlitedataProvider.searchOrNot(params);
    this.navCtrl.push(ProductDetailsPage, params);
  }

  productDetailsHistory(productId) {
    this.searchFilteredArray = this.searchData.filter(function (products) {
      console.log(products['productId'] == productId);
      return products['productId'] == productId;
    });

    let params = {
      'product_id': productId,
      'productName': this.searchFilteredArray[0].name,
      'productDescription': this.searchFilteredArray[0].productDescription,
      'old_price': this.searchFilteredArray[0].old_price,
      'price': this.searchFilteredArray[0].price,
      'categoryId': this.searchFilteredArray[0].categoryId,
      'categoryName': this.searchFilteredArray[0].categoryName,
      'color': this.searchFilteredArray[0].color,
      'size': this.searchFilteredArray[0].siz_e,
      'image_url': this.searchFilteredArray[0].image_url,
      'rate': this.searchFilteredArray[0].rate,
      'currency_symbol': this.settings.currency_symbol,
    };

    this.navCtrl.push(ProductDetailsPage, params);
  }

  loadSearchData() {
    this.SqlitedataProvider.loadSearchData().then((data) => {
      this.searchData = data;
    })
  }


  deleteSearch() {
    this.SqlitedataProvider.deleteSearch();
    this.searchData.length = 0;
  }


}
