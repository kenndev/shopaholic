import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {ProductsPage} from "../products/products";

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  categoryArray: any = [];
  data: any;
  errorMessage: string;
  doubleCategories: any = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dataprovider: DataProvider,) {
  }

  ionViewDidLoad() {
    this.getCategories();
  }

  getCategories() {

    this.dataprovider.getCategoriesAll()
      .subscribe(
        res => {

          this.data = res;
          this.categoryArray = this.data.category;
          this.getDoubleColumn();

        },
        error => this.errorMessage = <any>error);
  }

  getDoubleColumn() {
    let length = this.categoryArray.length;
    for (let i = 0; i < length; i += 2) {
      let trio = [];
      trio.push(this.categoryArray[i]);
      if (i + 1 < length) {
        trio.push(this.categoryArray[i + 1]);
      }
      this.doubleCategories.push(trio);
    }
    return this.doubleCategories;
  }


  listProducts(id, catName){
    this.navCtrl.push(ProductsPage, {categoryId: id, categoryName: catName});
  }

}
