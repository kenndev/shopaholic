import {Component} from '@angular/core';
import {AlertController, ModalController, NavController, NavParams} from 'ionic-angular';
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {Events} from 'ionic-angular';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {LoginPage} from "../login/login";
import {AngularFireAuth} from "angularfire2/auth";
import {CheckoutPage} from "../checkout/checkout";
import {ProductDetailsPage} from "../product-details/product-details";
import {OrderDetailsItemPage} from "../order-details-item/order-details-item";

@Component({
  selector: 'page-contact',
  templateUrl: 'cart.html'
})
export class CartPage {
  cartItems: any = [];
  count_cart: any;
  final_total: number = 0;
  isModal: boolean = false;
  productsFilteredArray: any = [];

  constructor(public navCtrl: NavController,
              private sqliteDataProvider: SqlitedataProvider,
              public events: Events,
              private params: NavParams,
              public modalCtrl: ModalController,
              private afAuth: AngularFireAuth,
              public alertCtrl: AlertController,) {
    this.isModal = params.get('isModal');

  }


  ionViewDidEnter() {

    this.countCart();
    this.cartChanged();
    this.countCartTotal();
    this.loadCartData();
  }

  cartChanged() {
    this.events.subscribe('cart:added', (cart) => {
      console.log('Cart', cart);
      if (cart == 1) {
        this.loadCartData();
        this.countCart();
      }
    });
  }


  loadCartData() {
    this.sqliteDataProvider.loadCartData().then((cartData) => {
      this.cartItems = cartData;
    })
    // this.cartItems = this.sqliteDataProvider.loadCartData();
  }

  countCart() {
    this.sqliteDataProvider.countCart().then((countCart) => {
      this.count_cart = countCart;
    });

    this.sqliteDataProvider.countCartLabelTabs();
  }

  add(event, id, quantity) {
    event.stopPropagation();
    let new_quantity = quantity + 1;
    this.sqliteDataProvider.updateQuantityCart(id, new_quantity);
    this.loadCartData();
    this.countCartTotal();
  }

  minus(event, id, quantity) {
    event.stopPropagation();
    if (quantity > 1) {
      let new_quantity = quantity - 1;
      this.sqliteDataProvider.updateQuantityCart(id, new_quantity);
      this.loadCartData();
      this.countCartTotal();
    }

  }

  deleteFromCart(event,id) {
    event.stopPropagation();
    this.sqliteDataProvider.deleteCartRecord(id);
    this.loadCartData();
    this.countCart();
    this.countCartTotal();
  }

  countCartTotal() {
    this.sqliteDataProvider.countTotal().then((total) => {
      this.final_total = total;
    });
  }

  closeModal() {
    this.navCtrl.pop();
  }

  isEmptyCart() {
    if (this.cartItems.length == 0) {
      return true;
    } else {
      return false;
    }
  }

  checkOut() {
    const unsubscribe = this.afAuth.auth.onAuthStateChanged(user => {
      if (!user) {

        this.presentDialog();
        unsubscribe();
      } else {
        this.navCtrl.push(CheckoutPage);
        unsubscribe();
      }
    });
  }

  presentDialog() {
    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: ' You need to be logged in to checkout. Proceed to login.',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let profileModal = this.modalCtrl.create(LoginPage, {isModal: true});
            profileModal.present();
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  productDetails(product_id) {

    this.productsFilteredArray = this.cartItems.filter(function (products) {
      console.log(products['product_id'] == product_id);
      return products['product_id'] == product_id;
    });

    let params = {
      'product_id': product_id,
      'productName': this.productsFilteredArray[0].name,
      'productDescription': this.productsFilteredArray[0].productDescription,
      'old_price': this.productsFilteredArray[0].old_price,
      'price': this.productsFilteredArray[0].price,
      'categoryId': this.productsFilteredArray[0].categoryId,
      'categoryName': this.productsFilteredArray[0].categoryName,
      'color': this.productsFilteredArray[0].color,
      'size': this.productsFilteredArray[0].siz_e,
      'image_url': this.productsFilteredArray[0].image_url,
      'rate': this.productsFilteredArray[0].rate,
      'currency_symbol': this.productsFilteredArray[0].currency_symbol,
    };
    this.navCtrl.push(OrderDetailsItemPage, params);
  }

}
