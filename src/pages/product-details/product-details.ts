import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {Events} from 'ionic-angular';
import {ToastController} from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import {CartPage} from "../cart/cart";
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";


/**
 * Generated class for the ProductDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {
  product_id: number;
  productName: string;
  productDescription: string;
  categoryName: string;
  old_price: string;
  price: string;
  image: string;
  sizeArray: any = [];
  sizeArrayRaw: any = [];
  categoryId: string;
  data: any;
  imageArray: any = [];
  colorArray: any = [];
  colorArrayRaw: any = [];
  errorMessage: string;
  hide: boolean = false;
  color: string;
  size: string;
  cartItemsNumber: String;
  wishlisted: any = [];
  rate: number;
  isWishlisted: boolean = false;
  currency_symbol: string;


  sizeOptions = {
    size: '',
  }

  colorOptions = {
    color: '',
  }

  constructor(public navCtrl: NavController,
              public dataprovider: DataProvider,
              public navParams: NavParams,
              public sqliteDataProvider: SqlitedataProvider,
              public events: Events,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController,
              private sqlite: SQLite,
              public alertCtrl: AlertController) {
  }

  ionViewDidEnter() {
    this.sqliteDataProvider.countCartLabelTabs();
    this.loadWishListData();
    this.events.subscribe('countCart', (cart) => {


      this.cartItemsNumber = cart;
    });
    this.product_id = this.navParams.get('product_id');
    this.productName = this.navParams.get('productName');
    this.productDescription = this.navParams.get('productDescription');
    this.old_price = this.navParams.get('old_price');
    this.price = this.navParams.get('price');
    this.categoryId = this.navParams.get('categoryId');
    this.categoryName = this.navParams.get('categoryName');
    this.sizeArrayRaw = this.navParams.get('size');
    this.sizeArray = JSON.parse(this.sizeArrayRaw);
    this.colorArrayRaw = this.navParams.get('color');
    this.colorArray = JSON.parse(this.colorArrayRaw);
    this.image = this.navParams.get('image_url');
    this.getProductsImages(this.product_id);
    this.rate = this.navParams.get('rate');
    this.currency_symbol = this.navParams.get('currency_symbol');

    if (this.colorArray.length > 0 || this.sizeArray.length > 0) {
      this.hide = true;
    } else {
      this.hide = false;
    }
  }

  loadWishListData() {

    this.sqliteDataProvider.loadWishListData().then((data) => {
      this.checkIfWishlisted(data);
    });

  }


  checkIfWishlisted(A) {
    var i, j;
    var totalmatches = 0;

    for (j = 0; j < A.length; ++j) {
      if (this.product_id == A[j]['productId']) {
        this.isWishlisted = true;
        totalmatches++;
      }

    }

  }


  getProductsImages(id) {
    this.dataprovider.getProductsImages(id)
      .subscribe(
        res => {
          this.data = res;
          this.imageArray = this.data.images;
        },
        error => this.errorMessage = <any>error);
  }

  starClicked(value) {
    console.log("Rated :", value);
  }

  printRating(rating) {

    let max_rate = 5;
    let rounded_rating = Math.round(rating);
    let array_stars = new Array(max_rate);
    array_stars.fill('star-outline');

    for (let i = 0; i < rounded_rating; i++) {
      array_stars[i] = 'star';

      if (i === rounded_rating - 1 && rating % 1 !== 0) {
        array_stars[i] = 'star-half';
      }
    }

    return array_stars;
  }

  addtocart(productId) {


    let newItem = {
      item_id: Math.random().toString(36).substr(2, 9),
      product_id: productId,
      productName: this.productName,
      productDescription: this.productDescription,
      old_price: this.old_price,
      price: this.price,
      categoryId: this.categoryId,
      categoryName: this.categoryName,
      size: this.sizeOptions.size,
      color: this.colorOptions.color,
      quantity: 1,
      image_url: this.image,
      rate: this.rate,
      currency_symbol: this.currency_symbol
    };

    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'Add ' + newItem['productName'] + ' to cart',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.sqliteDataProvider.addToCart(newItem);
            this.presentToast(newItem['productName'] + " has been added to cart");
            this.cartChanged();
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();


  }

  cartChanged() {
    this.sqliteDataProvider.countCartLabelTabs();
    this.events.publish('cart:added', 1);
  }


  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  cart() {
    let profileModal = this.modalCtrl.create(CartPage, {isModal: true});
    profileModal.present();
  }

  wishList(productId) {
    let message;
    if (this.isWishlisted) {

      this.isWishlisted = false;

      message = " had been REMOVED from wish list"
    } else {
      this.isWishlisted = true;
      message = " has been ADDED to wish list"
    }

    let newItem = {
      item_id: Math.random().toString(36).substr(2, 9),
      product_id: productId,
      productName: this.productName,
      productDescription: this.productDescription,
      old_price: this.old_price,
      price: this.price,
      categoryId: this.categoryId,
      categoryName: this.categoryName,
      size: this.sizeArrayRaw,
      color: this.colorArrayRaw,
      quantity: 1,
      image_url: this.image,
      rate: this.rate,
      currency_symbol: this.currency_symbol,
    };

    this.sqliteDataProvider.wishListOrNot(newItem);

    this.presentToast(newItem['productName'] + message);

  }


}
