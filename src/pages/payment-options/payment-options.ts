import {Component} from '@angular/core';
import {
  AlertController, Events, IonicPage, LoadingController, ModalController, NavController, NavParams,
  ToastController
} from 'ionic-angular';
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {PayPal, PayPalConfiguration, PayPalPayment} from "@ionic-native/paypal";
import {Stripe} from '@ionic-native/stripe';
import {DataProvider} from "../../providers/data/data";
import {AngularFireAuth} from "angularfire2/auth";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the PaymentOptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-options',
  templateUrl: 'payment-options.html',
})
export class PaymentOptionsPage {
  paypall: boolean = false;
  stripe: boolean = false;
  uponDelivery: boolean = false;
  total_price: any;
  count_cart: any;
  data: any;
  errorMessage: any;
  checkOutArray: any = [];
  cartDataArray: any = [];
  cartDataJson: any;
  months: any = [];
  billingAddress: any = [];
  shippingAddress: any = [];
  settings:any = [];

  paymentOptions = {
    pay_options: '',
    card_no: '',
    expMonth: '',
    expYear: '',
    cv_c: ''
  }


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private payPal: PayPal,
              private stripePayment: Stripe,
              private dataProvider: DataProvider,
              private afAuth: AngularFireAuth,
              public loadingCtrl: LoadingController,
              public dataprovider: DataProvider,
              private toastCtrl: ToastController,
              public events: Events,
              public alertCtrl: AlertController,
              public sqliteDataProvider: SqlitedataProvider) {

    this.sqliteDataProvider.loadCartData().then((cartData) => {
      this.cartDataArray = cartData;
      this.cartDataJson = JSON.stringify(this.cartDataArray);
    });

  }

  ionViewDidLoad() {
    this.getSettings();
    this.getMonths();
    this.countCartTotal();
    this.countCart();
    this.getAddresses();
  }

  getSettings() {
    this.dataprovider.getSlider()
      .subscribe(
        res => {
          this.data = res;
          this.settings = {
            currency_symbol: this.data.settings.currency_symbol,
            currency_code: this.data.settings.currency_code,
          }
        },
        error => this.errorMessage = <any>error);

  }

  getMonths() {
    this.dataProvider.getMonths()
      .subscribe(
        res => {
          let data = res;
          this.months = data;

        },
        error => this.errorMessage = <any>error);
  }

  openPayPall() {
    if (this.paypall) {
      this.paypall = false;
    } else {
      this.paypall = true;
      this.stripe = false;
      this.uponDelivery = false;
    }
  }

  openStripe() {
    if (this.stripe) {
      this.stripe = false;
    } else {
      this.stripe = true;
      this.paypall = false;
      this.uponDelivery = false;
    }
  }

  openUponDelivery() {
    if (this.uponDelivery) {
      this.uponDelivery = false;
    } else {
      this.uponDelivery = true;
      this.paypall = false;
      this.stripe = false;
    }
  }

  countCartTotal() {
    this.sqliteDataProvider.countTotal().then((total) => {
      this.total_price = total;
    });
  }

  countCart() {
    this.sqliteDataProvider.countCart().then((countCart) => {
      this.count_cart = countCart;
    });
  }

  makePayment() {
    if (this.paymentOptions.pay_options == 'upon-delivery') {
      this.makePaymentUponDelivery();
    } else if (this.paymentOptions.pay_options == 'PayPal') {
      this.payPAll();
    } else if (this.paymentOptions.pay_options == 'Stripe') {
      if (!this.paymentOptions.card_no || !this.paymentOptions.expMonth || !this.paymentOptions.expYear || !this.paymentOptions.cv_c) {
        alert("Some card details are missing. Please fill in the details to proceed");
      } else {
        this.makeStripePayment();
      }

    }

  }

  getAddresses() {
    this.sqliteDataProvider.loadBillingAddress().then((billingAddress) => {
      this.billingAddress = billingAddress;
    });
    this.sqliteDataProvider.loadShippingAddress().then((shippingAddress) => {
      this.shippingAddress = shippingAddress
    });
  }

  checkOut(payment_id, stripe_token) {
    let user = this.afAuth.auth.currentUser;
    this.checkOutArray = {
      order_id: Math.random().toString(36).substr(2, 6),
      totalAmount: this.total_price,
      stripeToken: stripe_token,
      firebase_id: user.uid,
      payment_option: this.paymentOptions.pay_options,
      payment_id: payment_id,
      order_details: this.cartDataJson,
      no_of_items: this.cartDataArray.length,
      billing_address: this.billingAddress[0]['address_id'],
      shipping_address: this.shippingAddress[0]['address_id'],
      check_out_date: new Date().toLocaleString(),
    };

    let loading = this.loadingCtrl.create({
      content: 'Checking out, please wait',
      spinner: 'crescent'
    });
    loading.present();
    this.dataProvider.makePayment(this.checkOutArray).subscribe(res => {
      this.data = res;
      this.sqliteDataProvider.deleteCart();
      loading.dismiss();
      let confirm = this.alertCtrl.create({
        title: 'Alert!',
        message: this.data.message,
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              this.cartChanged();
              this.navCtrl.setRoot(TabsPage);

              console.log('Agree clicked');
            }
          }
        ]
      });
      confirm.present();

    }, error => {
      this.errorMessage = <any>error;
      loading.dismiss();
    });

  }

  cartChanged() {
    this.sqliteDataProvider.countCartLabelTabs();
    this.events.publish('cart:added', 1);
  }

  makePaymentUponDelivery() {

    this.checkOut("", '');

  }


  makeStripePayment() {
    this.stripePayment.setPublishableKey('pk_test_EZK54iEma1qSTSfcKB0MrEXl');

    let card = {
      number: this.paymentOptions.card_no,
      expMonth: parseInt(this.paymentOptions.expMonth),
      expYear: parseInt(this.paymentOptions.expYear),
      cvc: this.paymentOptions.cv_c
    };

    let loading = this.loadingCtrl.create({
      content: 'Processing card, please wait',
      spinner: 'crescent'
    });
    loading.present();
    this.stripePayment.createCardToken(card)
      .then(token => {
        let cardData = {
          stripeToken: token.id,
          amount: this.total_price,
        };
        loading.dismiss();
        this.checkOut('', token.id);

      })
      .catch(error => {
        alert(error);
        loading.dismiss();
      });

  }

  payPAll() {
    let curr = this.settings.currency_code;
    this.payPal.init({
      PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
      PayPalEnvironmentSandbox: 'AZtgBLfIiVg8t8EHzzz6qb9cAcpaMUNJZ6aOj3mQYyCOrrJGzUI7XiK3ah_cWeIMqWfynFf2x4vwbpOX'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(this.total_price, "'" +curr+ "'", 'Description', 'sale');
        this.payPal.renderSinglePaymentUI(payment).then((res) => {
          // Successfully paid
          //alert(res.response.id);

          this.checkOut(res.response.id, '');

          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, (error1) => {
          alert(error1);
          // Error or render dialog closed without being successful
        });
      }, (error2) => {
        alert(error2);
        // Error in configuration
      });
    }, (error3) => {
      alert(error3);
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }


}
