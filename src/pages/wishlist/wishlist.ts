import {Component} from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {Events} from 'ionic-angular';
import {ProductDetailsPage} from "../product-details/product-details";

@Component({
  selector: 'page-about',
  templateUrl: 'wishlist.html'
})
export class WishlistPage {
  wishList: any = [];
  productsFilteredArray: any [];
  posts: any = [];

  constructor(public navCtrl: NavController,
              private sqliteDataProvider: SqlitedataProvider,
              public alertCtrl: AlertController,
              public events: Events) {

  }

  ionViewDidEnter() {
    this.loadWishListData2();
  }


  loadWishListData2() {
    this.sqliteDataProvider.loadWishListData().then((wishlist) => {
      this.wishList = wishlist;
    });
  }

  getDoubleColumn() {
    let triples = [];
    let length = this.wishList.length;
    for (let i = 0; i < length; i += 2) {
      let trio = [];
      trio.push(this.wishList[i]);
      if (i + 1 < length) {
        trio.push(this.wishList[i + 1]);
      }
      // if (i + 2 < length) {
      //   trio.push(this.wishList[i + 2]);
      // }

      triples.push(trio);
    }
    return triples;
  }

  printRating(rating) {

    let max_rate = 5;
    let rounded_rating = Math.round(rating);
    let array_stars = new Array(max_rate);
    array_stars.fill('star-outline');

    for (let i = 0; i < rounded_rating; i++) {
      array_stars[i] = 'star';

      if (i === rounded_rating - 1 && rating % 1 !== 0) {
        array_stars[i] = 'star-half';
      }
    }

    return array_stars;
  }

  productDetails(product_id) {

    this.productsFilteredArray = this.wishList.filter(function (products) {
      console.log(products['productId'] == product_id);
      return products['productId'] == product_id;
    });

    let params = {
      'product_id': product_id,
      'productName': this.productsFilteredArray[0].name,
      'productDescription': this.productsFilteredArray[0].productDescription,
      'old_price': this.productsFilteredArray[0].old_price,
      'price': this.productsFilteredArray[0].price,
      'categoryId': this.productsFilteredArray[0].categoryId,
      'categoryName': this.productsFilteredArray[0].categoryName,
      'color': this.productsFilteredArray[0].color,
      'size': this.productsFilteredArray[0].siz_e,
      'image_url': this.productsFilteredArray[0].image_url,
      'rate': this.productsFilteredArray[0].rate,
      'currency_symbol': this.productsFilteredArray[0].currency_symbol,
    };
    this.navCtrl.push(ProductDetailsPage, params);
  }

  calculatePercentageDiscount(oldPrice, newPrice) {
    let percentage;
    percentage = +(((oldPrice - newPrice) / oldPrice) * 100).toFixed(2);
    return percentage + " % off";
  }


  removeFromWishlist(deleteevent, id) {
    deleteevent.stopPropagation();
    let wishlistData = {
      product_id: id,
    }

    this.sqliteDataProvider.wishListOrNot(wishlistData);
    this.wishList = this.wishList.filter(function (el) {
      return el.productId !== id;
    });

  }

}
