import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  Loading,
  LoadingController,
  AlertController,
  NavParams, ToastController
} from 'ionic-angular';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {EmailValidator} from "../../validators/email";
import {FirebaseProvider} from "../../providers/firebase/firebase";
import {CheckoutPage} from "../checkout/checkout";
import {SignupPage} from "../signup/signup";
import {ResetPasswordPage} from "../reset-password/reset-password";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public loginForm: FormGroup;
  public loading: Loading;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public firebaseProvider: FirebaseProvider,
              public formBuilder: FormBuilder,
              public sqliteDatabaseProvider: SqlitedataProvider,
              private toastCtrl: ToastController,
              public navParams: NavParams) {

    this.loginForm = formBuilder.group({
      email: ['',
        Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['',
        Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }


  closeModal() {
    this.navCtrl.pop();
  }

  loginUser(): void {
    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);
    } else {
      this.loading = this.loadingCtrl.create({
        spinner: 'dots'
      });
      this.loading.present();
      this.firebaseProvider.loginUser(this.loginForm.value.email,
        this.loginForm.value.password)
        .then(authData => {
          let profile = {
            email: authData.email,
            uid: authData.uid,
            image: "",
            display_name: "",
          };
          this.sqliteDatabaseProvider.addProfile(profile);

          this.loading.dismiss();
          this.navCtrl.pop();
          this.presentToast("Login successful. Enjoy shopping with us")
        }, error => {
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();

        });
    }
  }

  goToSignup(): void {
    this.navCtrl.push(SignupPage);
  }

  goToResetPassword(): void {
    this.navCtrl.push(ResetPasswordPage);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
