import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {ProductDetailsPage} from "../product-details/product-details";
import {ToastController} from 'ionic-angular';
import {CategoryPage} from "../category/category";
import {ProductsPage} from "../products/products";
import {FilterPage} from "../filter/filter";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public data: any;
  public errorMessage: string;
  public sliderArray: any = [];
  public categoryArray: any = [];
  public productsArray: any = [];
  public productsFilteredArray: any = [];
  wishlisted: any = [];
  public stars: any = [];
  settings: any = [];
  c: any = [];

  constructor(public navCtrl: NavController,
              public dataprovider: DataProvider,
              private toastCtrl: ToastController,
              public sqliteDataProvider: SqlitedataProvider) {

  }

  ionViewDidEnter() {
    this.loadWishListData();
    this.getSlidersTrends();
  }

  calculatePercentageDiscount(oldPrice, newPrice) {
    let percentage;
    percentage = +(((oldPrice - newPrice) / oldPrice) * 100).toFixed(2);
    return percentage + " % off";
  }

  loadWishListData() {
    this.sqliteDataProvider.loadWishListData().then((wishListData) => {
      this.wishlisted = wishListData;
      this.getCategoriesProducts();
    });

  }


  getSlidersTrends() {
    this.dataprovider.getSlider()
      .subscribe(
        res => {
          this.data = res;
          this.sliderArray = this.data.slider;
          this.settings = {
            site_name: this.data.settings.site_name,
            currency_symbol: this.data.settings.currency_symbol,
          }
        },
        error => this.errorMessage = <any>error);


  }


  getCategoriesProducts() {

    this.dataprovider.getProductsAndCategriesHome()
      .subscribe(
        res => {

          this.data = res;
          this.categoryArray = this.data.category;
          this.productsArray = this.data.products;
          this.checkIfWishlisted(this.wishlisted);


        },
        error => this.errorMessage = <any>error);
  }

  checkIfWishlisted(A) {
    var myarr = this.productsArray;
    var i, j;
    var totalmatches = 0;
    for (i = 0; i < myarr.length; i++) {
      for (j = 0; j < A.length; ++j) {
        if (myarr[i]['productId'] == A[j]['productId']) {
          myarr[i]['isWishlisted'] = true;
          totalmatches++;
        }

      }
    }
  }


  printRating(rating) {

    let max_rate = 5;
    let rounded_rating = Math.round(rating);
    let array_stars = new Array(max_rate);
    array_stars.fill('star-outline');

    for (let i = 0; i < rounded_rating; i++) {
      array_stars[i] = 'star';

      if (i === rounded_rating - 1 && rating % 1 !== 0) {
        array_stars[i] = 'star-half';
      }
    }

    return array_stars;
  }

  productDetails(product_id) {

    this.productsFilteredArray = this.productsArray.filter(function (products) {
      console.log(products['productId'] == product_id);
      return products['productId'] == product_id;
    });

    let params = {
      'product_id': product_id,
      'productName': this.productsFilteredArray[0].productName,
      'productDescription': this.productsFilteredArray[0].productDescription,
      'old_price': this.productsFilteredArray[0].old_price,
      'price': this.productsFilteredArray[0].price,
      'categoryId': this.productsFilteredArray[0].categoryId,
      'categoryName': this.productsFilteredArray[0].category_name,
      'color': this.productsFilteredArray[0].color,
      'size': this.productsFilteredArray[0].size,
      'image_url': this.productsFilteredArray[0].image,
      'rate': this.productsFilteredArray[0].rate,
      'currency_symbol': this.settings.currency_symbol,
    };
    this.navCtrl.push(ProductDetailsPage, params);
  }


  wishlist(event, product) {
    event.stopPropagation();
    let message;
    if (product['isWishlisted']) {

      product['isWishlisted'] = false;

      message = " has been REMOVED from wish list"
    } else {
      product['isWishlisted'] = true;
      message = " has been ADDED to wish list"
    }

    let newItem = {
      item_id: Math.random().toString(36).substr(2, 9),
      product_id: product['productId'],
      productName: product['productName'],
      productDescription: product['productDescription'],
      old_price: product['old_price'],
      price: product['price'],
      categoryId: product['categoryId'],
      categoryName: product['category_name'],
      size: product['size'],
      color: product['color'],
      image_url: product['image'],
      rate: product['rate'],
      currency_symbol: this.settings.currency_symbol,

    };

    this.sqliteDataProvider.wishListOrNot(newItem);

    this.presentToast(newItem['productName'] + message);

  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  allCategories() {
    this.navCtrl.push(CategoryPage);
  }

  specificCategory(id, catName) {
    this.navCtrl.push(ProductsPage, {categoryId: id, categoryName: catName});
  }

  filter() {
    this.navCtrl.push(FilterPage);
  }

  //Check if array is empty or not and display no records available image if empty
  getState(id) {
    let filter_array = this.productsArray.filter(function (products) {
      return products['categoryId'] == id;
    });

    if (filter_array.length > 0) {
      return true
    } else {
      return false;
    }
  }


}
