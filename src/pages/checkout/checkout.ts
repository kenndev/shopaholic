import {Component} from '@angular/core';
import {Events, IonicPage, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {AngularFireAuth} from "angularfire2/auth";
import {DataProvider} from "../../providers/data/data";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {BillingAddressPage} from "../billing-address/billing-address";


/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  data: any;
  errorMessage: string;
  loading: any;
  shippingAddressArray: any = [];
  billingAddressArray: any = [];
  billingAddressEmpty: boolean = false;
  shippingAdressEmpty: boolean = false;
  billing: boolean = false;
  shipping: boolean = false;


  constructor(public navCtrl: NavController,
              private afAuth: AngularFireAuth,
              public modalCtrl: ModalController,
              public events: Events,
              public dataProvider: DataProvider,
              public loadingCtrl: LoadingController,
              private sqliteDataProvider: SqlitedataProvider,
              public navParams: NavParams) {
  }

  ionViewDidEnter() {
    this.getAddressFromSqlite();
    this.events.subscribe('shipping:added', () => {
      this.shippingAdressEmpty = false;
    });
  }


  getAddressFromSqlite() {
    this.sqliteDataProvider.loadShippingAddress().then((res) => {
      this.shippingAddressArray = res;
      if (res.length == 0) {
        this.shipping = false;
      } else {
        this.shippingAddressArray = res;
        this.shipping = true;
      }
    });

    this.sqliteDataProvider.loadBillingAddress().then((billingRes) => {
      this.billingAddressArray = billingRes;
      if (billingRes.length == 0) {
        this.billing = false;
      } else {
        this.billingAddressArray = billingRes;
        this.billing = true;
      }
    });

    if (!this.shipping || !this.billing) {
      this.getAddress();
    }
  }

  getAddress() {
    let user = this.afAuth.auth.currentUser;

    this.loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    this.loading.present();
    this.getShippingBillingAddress(user.uid);
  }

  getShippingBillingAddress(uid) {
    this.dataProvider.getShippingBillingAdress(uid)
      .subscribe(
        res => {
          this.data = res;

          if (!this.shipping) {
            if (!this.data.shippingempty) {
              //Insert into shipping_address
              let shippingData = {
                address_id: this.data.shipping.id,
                first_name: this.data.shipping.first_name,
                last_name: this.data.shipping.last_name,
                phone_number: this.data.shipping.phone_number,
                country: this.data.shipping.country,
                state: this.data.shipping.state,
                town: this.data.shipping.town,
                address: this.data.shipping.address,
                email: this.data.shipping.email,

              };
              this.sqliteDataProvider.addShipping(shippingData);
              //Retrieve shipping address array from shipping_address sqlite
              this.sqliteDataProvider.loadShippingAddress().then((res) => {

                this.shippingAddressArray = res;

              });
              //Set this to true to display information
              this.shippingAdressEmpty = false;
            } else {
              this.shippingAdressEmpty = true;
            }
          }

          if (!this.billing) {
            if (!this.data.billingempty) {
              //Insert into billing_address
              let billingData = {
                address_id: this.data.billing.id,
                first_name: this.data.billing.first_name,
                last_name: this.data.billing.last_name,
                phone_number: this.data.billing.phone_number,
                country: this.data.billing.country,
                state: this.data.billing.state,
                town: this.data.billing.town,
                address: this.data.billing.address,
                email: this.data.billing.email,

              };

              this.sqliteDataProvider.addBilling(billingData);
              //Retrieve shipping address array from billing_address sqlite
              this.sqliteDataProvider.loadBillingAddress().then((res) => {

                this.billingAddressArray = res;

              });
              //Set this to true to display information
              this.billingAddressEmpty = false;
            } else {
              this.billingAddressEmpty = true;
            }
          }

          this.loading.dismiss();
        },
        error => {this.errorMessage = <any>error;this.loading.dismiss();});


  }

  addBillingAddress() {
    this.navCtrl.push("BillingAddressPage");
  }

  addShippingAddress() {
    this.navCtrl.push("ShippingAdressPage");
  }

  selectPaymentOption() {
    this.navCtrl.push("PaymentOptionsPage");
  }

  selectBillingAddress() {
    this.navCtrl.push('BillingPageListPage');
  }

  selectShippingAddress() {
    this.navCtrl.push('ShippingPageListPage');
  }


}
