import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {OrderDetailsItemPage} from "../order-details-item/order-details-item";

/**
 * Generated class for the OrderDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
})
export class OrderDetailsPage {
  orderId: number;
  data: any;
  errorMessage: any;
  orderItems: any = [];
  productsFilteredArray: any = [];
  settings: any = [];


  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public dataProvider: DataProvider,
              private toastCtrl: ToastController,
              public navParams: NavParams) {


  }

  ionViewDidLoad() {
    this.orderId = this.navParams.get('orderId');
    this.getOrdersItems(this.orderId);
  }

  getDoubleColumn() {
    let triples = [];
    let length = this.orderItems.length;
    for (let i = 0; i < length; i += 2) {
      let trio = [];
      trio.push(this.orderItems[i]);
      if (i + 1 < length) {
        trio.push(this.orderItems[i + 1]);
      }
      triples.push(trio);
    }
    return triples;
  }

  getOrdersItems(order_id) {
    let loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    loading.present();
    this.dataProvider.getOrderItems(order_id)
      .subscribe(
        res => {
          this.data = res;
          this.orderItems = this.data.orderDetails;
          this.settings = {
            currency_symbol: this.data.settings.currency_symbol
          }
          this.presentToast(this.data.message);
          loading.dismiss();
        },
        error => {
          this.errorMessage = <any>error;
          loading.dismiss();
        });

  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  calculatePercentageDiscount(oldPrice, newPrice) {
    let percentage;
    percentage = +(((oldPrice - newPrice) / oldPrice) * 100).toFixed(2);
    return percentage + " % off";
  }

  printRating(rating) {

    let max_rate = 5;
    let rounded_rating = Math.round(rating);
    let array_stars = new Array(max_rate);
    array_stars.fill('star-outline');

    for (let i = 0; i < rounded_rating; i++) {
      array_stars[i] = 'star';

      if (i === rounded_rating - 1 && rating % 1 !== 0) {
        array_stars[i] = 'star-half';
      }
    }

    return array_stars;
  }

  productDetails(product_id) {
    this.productsFilteredArray = this.orderItems.filter(function (products) {
      console.log(products['productId'] == product_id);
      return products['productId'] == product_id;
    });

    let params = {
      'product_id': product_id,
      'productName': this.productsFilteredArray[0].productName,
      'productDescription': this.productsFilteredArray[0].productDescription,
      'old_price': this.productsFilteredArray[0].old_price,
      'price': this.productsFilteredArray[0].price,
      'categoryId': this.productsFilteredArray[0].categoryId,
      'categoryName': this.productsFilteredArray[0].category_name,
      'color': this.productsFilteredArray[0].color,
      'size': this.productsFilteredArray[0].size,
      'image_url': this.productsFilteredArray[0].image,
      'rate': this.productsFilteredArray[0].rate,
      'currency_symbol': this.settings.currency_symbol,
    };
    this.navCtrl.push(OrderDetailsItemPage, params);
  }

}
