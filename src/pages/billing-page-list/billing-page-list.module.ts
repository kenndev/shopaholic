import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillingPageListPage } from './billing-page-list';

@NgModule({
  declarations: [
    BillingPageListPage,
  ],
  imports: [
    IonicPageModule.forChild(BillingPageListPage),
  ],
})
export class BillingPageListPageModule {}
