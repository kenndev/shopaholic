import {Component} from '@angular/core';
import {
  AlertController, Events, IonicPage, LoadingController, ModalController, NavController, NavParams,
  ToastController
} from 'ionic-angular';
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {DataProvider} from "../../providers/data/data";
import {AngularFireAuth} from "angularfire2/auth";
import {LoginPage} from "../login/login";

/**
 * Generated class for the BillingPageListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-billing-page-list',
  templateUrl: 'billing-page-list.html',
})
export class BillingPageListPage {
  billingAddressArray: any = [];
  billingAddressArrayFiltered: any = [];
  data: any;
  errorMessage: any;

  constructor(public navCtrl: NavController,
              private sqliteDataProvider: SqlitedataProvider,
              private dataProvider: DataProvider,
              private afAuth: AngularFireAuth,
              public loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController,
              public events: Events,
              public alertCtrl: AlertController,

              public navParams: NavParams) {
  }

  ionViewDidEnter() {
    let user = this.afAuth.auth.currentUser;
    if (user) {
      this.loadBillingAddress();
    } else {
      this.presentAlert();
    }
  }

  presentAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'You need to be logged in to view this page. Proceed to login.',
      buttons: [
        {
          text: 'No',
          handler: () => {

            this.navCtrl.pop();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let profileModal = this.modalCtrl.create(LoginPage, {isModal: true});
            profileModal.onDidDismiss(() => {
              // Call the method to do whatever in your home.ts

              this.loadBillingAddress();
            });
            profileModal.present();
          }
        }
      ]
    });
    confirm.present();
  }

  loadBillingAddress() {
    let loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    loading.present();
    let user = this.afAuth.auth.currentUser;
    this.dataProvider.getAddress(user.uid)
      .subscribe(
        res => {

          this.data = res;
          this.billingAddressArray = this.data.billing;
          this.presentToast(this.data.message);
          loading.dismiss();

        },
        error => {
          this.errorMessage = <any>error;
          loading.dismiss();
        });


  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  addBillingAddress() {
    this.navCtrl.push('BillingAddressPage');
  }

  chooseBillingAddress(id) {
    let user = this.afAuth.auth.currentUser;
    this.billingAddressArrayFiltered = this.billingAddressArray.filter(function (address) {
      return address['id'] == id;
    });

    let shippingData = {
      address_id: this.billingAddressArrayFiltered[0]['id'],
      first_name: this.billingAddressArrayFiltered[0]['first_name'],
      last_name: this.billingAddressArrayFiltered[0]['last_name'],
      phone_number: this.billingAddressArrayFiltered[0]['phone_number'],
      country: this.billingAddressArrayFiltered[0]['country'],
      state: this.billingAddressArrayFiltered[0]['state'],
      town: this.billingAddressArrayFiltered[0]['town'],
      address: this.billingAddressArrayFiltered[0]['address'],
      email: this.billingAddressArrayFiltered[0]['email'],
      firebase_id: user.uid,

    };

    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'Choose this address as your primary billing address?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.sqliteDataProvider.deleteBilling();
            this.sqliteDataProvider.addBilling(shippingData);
            this.presentToast("The address has been set as the primary address successfully");
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();

  }


}
