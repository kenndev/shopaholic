import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingPageListPage } from './shipping-page-list';

@NgModule({
  declarations: [
    ShippingPageListPage,
  ],
  imports: [
    IonicPageModule.forChild(ShippingPageListPage),
  ],
})
export class ShippingPageListPageModule {}
