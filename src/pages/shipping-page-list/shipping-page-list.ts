import {Component} from '@angular/core';
import {
  AlertController, Events, IonicPage, LoadingController, ModalController, NavController, NavParams,
  ToastController
} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {SqlitedataProvider} from "../../providers/sqlitedata/sqlitedata";
import {DataProvider} from "../../providers/data/data";
import {AngularFireAuth} from "angularfire2/auth";

/**
 * Generated class for the ShippingPageListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shipping-page-list',
  templateUrl: 'shipping-page-list.html',
})
export class ShippingPageListPage {

  data: any;
  errorMessage: any;
  shippingAddressArray: any = [];
  shippingAddressArrayFiltered: any = [];

  constructor(public navCtrl: NavController,
              private sqliteDataProvider: SqlitedataProvider,
              private dataProvider: DataProvider,
              private afAuth: AngularFireAuth,
              public loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController,
              public events: Events,
              public alertCtrl: AlertController,
              public navParams: NavParams) {
  }

  ionViewDidEnter() {
    let user = this.afAuth.auth.currentUser;
    if (user) {
      this.loadShippingAddress();
    } else {
      this.presentAlert();
    }
  }

  presentAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'You need to be logged in to view this page. Proceed to login.',
      buttons: [
        {
          text: 'No',
          handler: () => {

            this.navCtrl.pop();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let profileModal = this.modalCtrl.create(LoginPage, {isModal: true});
            profileModal.onDidDismiss(() => {
              // Call the method to do whatever in your home.ts

              this.loadShippingAddress();
            });
            profileModal.present();
          }
        }
      ]
    });
    confirm.present();
  }

  loadShippingAddress() {
    let loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    loading.present();
    let user = this.afAuth.auth.currentUser;
    this.dataProvider.getAddress(user.uid)
      .subscribe(
        res => {

          this.data = res;
          this.shippingAddressArray = this.data.shipping;
          this.presentToast(this.data.message);
          loading.dismiss();

        },
        error => {
          this.errorMessage = <any>error;
          loading.dismiss();
        });


  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  addShippingAddress() {
    this.navCtrl.push('ShippingAdressPage');
  }

  chooseShippingAddress(id) {
    let user = this.afAuth.auth.currentUser;
    this.shippingAddressArrayFiltered = this.shippingAddressArray.filter(function (address) {
      return address['id'] == id;
    });

    let shippingData = {
      address_id: this.shippingAddressArrayFiltered[0]['id'],
      first_name: this.shippingAddressArrayFiltered[0]['first_name'],
      last_name: this.shippingAddressArrayFiltered[0]['last_name'],
      phone_number: this.shippingAddressArrayFiltered[0]['phone_number'],
      country: this.shippingAddressArrayFiltered[0]['country'],
      state: this.shippingAddressArrayFiltered[0]['state'],
      town: this.shippingAddressArrayFiltered[0]['town'],
      address: this.shippingAddressArrayFiltered[0]['address'],
      email: this.shippingAddressArrayFiltered[0]['email'],
      firebase_id: user.uid,

    };

    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'Choose this address as your primary shipping address?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.sqliteDataProvider.deleteShipping();
            this.sqliteDataProvider.addShipping(shippingData);
            this.events.publish('shipping:added');
            this.presentToast("The address has been set as the primary address successfully");
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();

  }

}
