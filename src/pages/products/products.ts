import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {ProductDetailsPage} from "../product-details/product-details";

/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  categoryId: number;
  categoryName: string;
  data: any;
  errorMessage: string;
  productsArray: any = [];
  doubleCategories: any = [];
  productsFilteredArray: any = [];
  settings: any = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dataprovider: DataProvider) {

  }

  ionViewDidLoad() {
    this.categoryId = this.navParams.get('categoryId');
    this.categoryName = this.navParams.get('categoryName');
    this.getProductsByCategoryId();

  }

  getProductsByCategoryId() {

    this.dataprovider.getProductsByCategory(this.categoryId)
      .subscribe(
        res => {

          this.data = res;
          this.productsArray = this.data.products;
          this.settings = {
            currency_symbol: this.data.settings.currency_symbol
          }
          this.getDoubleColumn();

        },
        error => this.errorMessage = <any>error);
  }

  getDoubleColumn() {
    let length = this.productsArray.length;
    for (let i = 0; i < length; i += 2) {
      let trio = [];
      trio.push(this.productsArray[i]);
      if (i + 1 < length) {
        trio.push(this.productsArray[i + 1]);
      }
      this.doubleCategories.push(trio);
    }
    return this.doubleCategories;
  }

  printRating(rating) {

    let max_rate = 5;
    let rounded_rating = Math.round(rating);
    let array_stars = new Array(max_rate);
    array_stars.fill('star-outline');

    for (let i = 0; i < rounded_rating; i++) {
      array_stars[i] = 'star';

      if (i === rounded_rating - 1 && rating % 1 !== 0) {
        array_stars[i] = 'star-half';
      }
    }

    return array_stars;
  }

  calculatePercentageDiscount(oldPrice, newPrice) {
    let percentage;
    percentage = +(((oldPrice - newPrice) / oldPrice) * 100).toFixed(2);
    return percentage + " % off";
  }

  productDetails(product_id) {

    this.productsFilteredArray = this.productsArray.filter(function (products) {
      console.log(products['productId'] == product_id);
      return products['productId'] == product_id;
    });

    let params = {
      'product_id': product_id,
      'productName': this.productsFilteredArray[0].productName,
      'productDescription': this.productsFilteredArray[0].productDescription,
      'old_price': this.productsFilteredArray[0].old_price,
      'price': this.productsFilteredArray[0].price,
      'categoryId': this.productsFilteredArray[0].categoryId,
      'categoryName': this.productsFilteredArray[0].categoryName,
      'color': this.productsFilteredArray[0].color,
      'size': this.productsFilteredArray[0].size,
      'image_url': this.productsFilteredArray[0].image,
      'rate': this.productsFilteredArray[0].rate,
      "currency_symbol": this.settings.currency_symbol,
    };
    this.navCtrl.push(ProductDetailsPage, params);
  }

}
