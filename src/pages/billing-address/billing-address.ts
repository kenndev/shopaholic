import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {AngularFireAuth} from "angularfire2/auth";
import {EmailValidator} from "../../validators/email";

/**
 * Generated class for the BillingAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-billing-address',
  templateUrl: 'billing-address.html',
})
export class BillingAddressPage {
  data: any;
  user: any;
  errorMessage: string;
  countries: any = [];
  public billingForm: FormGroup;


  constructor(public navCtrl: NavController,
              public dataProvider: DataProvider,
              private toastCtrl: ToastController,
              private afAuth: AngularFireAuth,
              public formBuilder: FormBuilder,
              public loadingCtrl: LoadingController,
              public navParams: NavParams) {
    this.billingForm = formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone_number: ['', Validators.required],
      post_code: [''],
      state: ['', Validators.required],
      town: ['', Validators.required],
      country: [''],
      email: ['',
        Validators.compose([Validators.required, EmailValidator.isValid])],
      address: [''],
    });
  }

  ionViewDidLoad() {
    this.getCountries();
  }

  getCountries() {
    this.dataProvider.getCountries()
      .subscribe(
        res => {
          let data = res;
          this.countries = data;

        },
        error => this.errorMessage = <any>error);
  }

  addBillingForm() {

    this.user = this.afAuth.auth.currentUser;

    let billingData = {
      first_name: this.billingForm.value.first_name,
      last_name: this.billingForm.value.last_name,
      phone_number: this.billingForm.value.phone_number,
      post_code: this.billingForm.value.post_code,
      state: this.billingForm.value.state,
      town: this.billingForm.value.town,
      country: this.billingForm.value.country,
      address: this.billingForm.value.address,
      email: this.billingForm.value.email,
      firebase_id: this.user.uid,

    }
    let loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    loading.present();
    this.dataProvider.saveBillingAddress(billingData).subscribe(res => {
      this.data = res;
      if (this.data.result == 'success') {
        loading.dismiss();
        this.presentToast(this.data.message);
        this.navCtrl.pop();
      } else {
        loading.dismiss();
        this.presentToast(this.data.message);
      }

    }, error => this.errorMessage = <any>error);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
