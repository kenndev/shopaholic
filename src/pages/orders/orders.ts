import {Component} from '@angular/core';
import {
  AlertController, IonicPage, LoadingController, ModalController, NavController, NavParams,
  ToastController
} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {AngularFireAuth} from "angularfire2/auth";
import {LoginPage} from "../login/login";

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  data: any;
  errorMessage: any;
  ordersArray: any = [];
  colorclass: any;
  productsArray: any = [];

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public dataProvider: DataProvider,
              private afAuth: AngularFireAuth,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    let user = this.afAuth.auth.currentUser;
    if (user) {
      this.getOrders();
    } else {
      this.presentAlert();
    }
  }

  presentAlert() {
    let confirm = this.alertCtrl.create({
      title: 'Alert!',
      message: 'You need to be logged in to view this page. Proceed to login.',
      buttons: [
        {
          text: 'No',
          handler: () => {
            this.navCtrl.pop();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let profileModal = this.modalCtrl.create(LoginPage, {isModal: true});
            profileModal.onDidDismiss(() => {
              // Call the method to do whatever in your home.ts

              this.getOrders();
            });
            profileModal.present();
          }
        }
      ]
    });
    confirm.present();
  }


  getOrders() {
    let loading = this.loadingCtrl.create({
      spinner: 'dots'
    });
    loading.present();
    let user = this.afAuth.auth.currentUser;
    this.dataProvider.getOrders(user.uid)
      .subscribe(
        res => {
          this.data = res;
          this.ordersArray = this.data.orders;
          this.presentToast(this.data.message);
          loading.dismiss();
        },
        error => {
          this.errorMessage = <any>error;
          loading.dismiss();
        });

  }

  loadItemData() {

  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  orderDetails(orderId) {
    let params = {
      orderId: orderId
    };
    this.navCtrl.push("OrderDetailsPage", params);

  }

}
