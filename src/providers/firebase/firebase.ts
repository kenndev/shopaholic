import {Injectable} from '@angular/core';

import {AngularFireAuth} from 'angularfire2/auth';
import {Headers, Http} from "@angular/http";
import {DataProvider} from "../data/data";
import {SqlitedataProvider} from "../sqlitedata/sqlitedata";

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {
  data: any;

  constructor(private afAuth: AngularFireAuth,
              private sqliteDatabaseProvider: SqlitedataProvider,
              private dataProvider: DataProvider) {
    console.log('Hello FirebaseProvider Provider');
  }

  loginUser(email: string, password: string): Promise<any> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  signupUser(email: string, password: string): Promise<any> {
    // return firebase
    //   .auth()
    //   .createUserWithEmailAndPassword(email, password)
    //   .then(newUser => {
    //     // firebase
    //     //   .database()
    //     //   .ref('/userProfile')
    //     //   .child(newUser.uid)
    //     //   .set({email: email});
    //   });

    return this.afAuth.auth.createUserWithEmailAndPassword(
      email,
      password
    ).then(newUser => {
      this.dataProvider.saveUser(newUser.email, newUser.uid).subscribe(res =>{
        this.data = res;
        let profile = {
          email: this.data.shopper.email,
          uid: this.data.shopper.firebase_id,
          image: this.data.shopper.image,
          display_name: this.data.shopper.display_name,
        };
        this.sqliteDatabaseProvider.addProfile(profile);
        alert(this.data.message);
      })
    });
  }

  resetPassword(email: string): Promise<void> {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  logoutUser(): Promise<void> {
    return this.afAuth.auth.signOut();
  }

}
