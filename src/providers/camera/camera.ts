import {Injectable} from '@angular/core';
import {Camera} from '@ionic-native/camera';
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {File} from "@ionic-native/file";
import {AngularFireAuth} from "angularfire2/auth";
import {LoadingController, ToastController} from "ionic-angular";
import {SqlitedataProvider} from "../sqlitedata/sqlitedata";

/*
  Generated class for the CameraProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CameraProvider {
  user: any;
  data: any;
  time: any;

  constructor(private camera: Camera,
              private transfer: FileTransfer,
              private afAuth: AngularFireAuth,
              private toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private sqliteDatabaseProvider: SqlitedataProvider,
              private file: File,) {

  }

  getPictureFromCamera() {
    return this.getImage(this.camera.PictureSourceType.CAMERA, true);
  }

  getPictureFromPhotoLibrary() {
    return this.getImage(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  // This method takes optional parameters to make it more customizable
  getImage(pictureSourceType, crop = true, quality = 50, allowEdit = true, saveToAlbum = true) {
    const options = {
      quality,
      allowEdit,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: pictureSourceType,
      encodingType: this.camera.EncodingType.PNG,
      saveToPhotoAlbum: saveToAlbum
    };

    // If set to crop, restricts the image to a square of 600 by 600
    if (crop) {
      options['targetWidth'] = 800;
      options['targetHeight'] = 800;
    }

    return this.camera.getPicture(options).then(imageData => {
      //const base64Image = 'data:image/png;base64,' + imageData;

      let imageUrl = imageData;
      const fileTransfer: FileTransferObject = this.transfer.create();
      this.user = this.afAuth.auth.currentUser;
      this.time = new Date();
      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: Math.round(this.time / 1000) + '.png',
        chunkedMode: false,
        mimeType: "image/png",
        params: {
          firebase_id: this.user.uid,
        },
        headers: {}
      }
      const loading = this.loadingCtrl.create();

      loading.present();
      fileTransfer.upload(imageUrl, 'http://shopaholic.allthingzcode.com/api/sm/updateProfilePic', options)
        .then((data2) => {
          loading.dismiss();
          this.presentToast("Profile picture updated successfully");
        }, (err) => {
          loading.dismiss();
          this.presentToast(err.body);
        })

      return imageUrl;
    }, error => {
      console.log('CAMERA ERROR -> ' + JSON.stringify(error));
    });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


}
