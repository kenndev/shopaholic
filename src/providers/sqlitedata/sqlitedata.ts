import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {Events} from 'ionic-angular';
import {Observable} from "rxjs/Observable";


/*
  Generated class for the SqlitedataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SqlitedataProvider {
  bdf: SQLiteObject;

  constructor(public http: Http,
              private sqlite: SQLite,
              public events: Events) {
  }

  createDatabase() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('CREATE TABLE IF NOT EXISTS cart (id INTEGER PRIMARY KEY AUTOINCREMENT,currency_symbol TEXT, item_id INTEGER, product_id INTEGER, productName TEXT, productDescription TEXT, old_price TEXT, price TEXT, categoryID INTEGER, categoryName TEXT, siz_e TEXT, color TEXT, quantity INTEGER, image_url TEXT, rate INTEGER)', {})
          .then(() => console.log('Executed SQL create'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }


  addToCart(cartDetails) {
    let item_id = cartDetails['item_id'];
    let product_id = cartDetails['product_id'];
    let productName = cartDetails['productName'];
    let productDescription = cartDetails['productDescription'];
    let old_price = cartDetails['old_price'];
    let price = cartDetails['price'];
    let categoryID = cartDetails['categoryID'];
    let categoryName = cartDetails['categoryName'];
    let size = cartDetails['size'];
    let color = cartDetails['color'];
    let quantity = cartDetails['quantity'];
    let image_url = cartDetails['image_url'];
    let rate = cartDetails['rate'];
    let currency_symbol = cartDetails['currency_symbol'];
    let cartData = [currency_symbol, item_id, product_id, productName, productDescription, old_price, price, categoryID, categoryName, size, color, quantity, image_url];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('INSERT INTO cart (currency_symbol,item_id,product_id,productName,productDescription,old_price,price,categoryID,categoryName,siz_e,color,quantity,image_url,rate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', cartData)
          .then(() => console.log('Executed SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));


  }


  loadCartData(): Promise<any[]> {
    let cart: any = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from cart ORDER BY id DESC', [])
          .then(data => {


            if (data.rows.length > 0) {

              cart.length = 0;
              for (var i = 0; i < data.rows.length; i++) {
                cart.push({
                  name: data.rows.item(i).productName,
                  image_url: data.rows.item(i).image_url,
                  price: data.rows.item(i).price,
                  old_price: data.rows.item(i).old_price,
                  quantity: data.rows.item(i).quantity,
                  product_id: data.rows.item(i).product_id,
                  siz_e: data.rows.item(i).siz_e,
                  color: data.rows.item(i).color,
                  id: data.rows.item(i).id,
                  categoryID: data.rows.item(i).categoryID,
                  categoryName: data.rows.item(i).categoryName,
                  rate: data.rows.item(i).rate,
                  productDescription: data.rows.item(i).productDescription,
                  currency_symbol: data.rows.item(i).currency_symbol,
                });


              }
            }
            return cart;
          })
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));

  }


  countTotal(): Promise<any> {

    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from cart ORDER BY id DESC', [])
          .then(data => {

            if (data.rows.length > 0) {
              var finaltotal: number = 0;
              var total = 0
              for (var i = 0; i < data.rows.length; i++) {
                total = data.rows.item(i).price * data.rows.item(i).quantity;
                finaltotal = total + finaltotal;

              }
              return finaltotal;
            }
          })
          .catch(e => alert(e));
      })
      .catch(e => alert(e));
  }


  updateQuantityCart(id, quantity) {
    let cartData = [quantity, id];
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('UPDATE cart SET quantity = ? WHERE id = ?', cartData)
          .then(() => console.log("Executed sql Update"))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  deleteCartRecord(id) {
    let cartData = [id];
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('DELETE FROM cart WHERE id = ?', cartData)
          .then(() => console.log("Executed sql Delete"))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  countCart(): Promise<any> {
    //let count = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select count(*) as cnt from cart', [])
          .then(data => {
            //cart_count = data.rows.item(0).cnt;
            // count.length = 0;
            // count.push({
            //   new_count: data.rows.item(0).cnt,
            // });
            let count = data.rows.item(0).cnt;

            return count;
          })
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  countCartLabelTabs() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('select count(*) as cnt from cart', [])
          .then(data => {

            this.events.publish('countCart', data.rows.item(0).cnt);


          })
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  deleteCart() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("DELETE FROM cart", []).then(results => {

        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }

  /**
   *
   * Wish list
   */
  loadWishListData(): Promise<any[]> {
    let cart: any = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from wishList ORDER BY id DESC', [])
          .then(data => {


            if (data.rows.length > 0) {

              cart.length = 0;
              for (var i = 0; i < data.rows.length; i++) {
                cart.push({
                  productId: data.rows.item(i).product_id,
                  name: data.rows.item(i).productName,
                  productDescription: data.rows.item(i).productDescription,
                  categoryId: data.rows.item(i).categoryId,
                  categoryName: data.rows.item(i).categoryName,
                  color: data.rows.item(i).color,
                  siz_e: data.rows.item(i).siz_e,
                  image_url: data.rows.item(i).image_url,
                  price: data.rows.item(i).price,
                  old_price: data.rows.item(i).old_price,
                  id: data.rows.item(i).id,
                  rate: data.rows.item(i).rate,
                  currency_symbol: data.rows.item(i).currency_symbol,
                });

              }
            }
            return cart;
          })
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  addtoWishlist(wishlistDetails) {
    let item_id = wishlistDetails['item_id'];
    let product_id = wishlistDetails['product_id'];
    let productName = wishlistDetails['productName'];
    let productDescription = wishlistDetails['productDescription'];
    let old_price = wishlistDetails['old_price'];
    let price = wishlistDetails['price'];
    let categoryID = wishlistDetails['categoryID'];
    let categoryName = wishlistDetails['categoryName'];
    let size = wishlistDetails['size'];
    let color = wishlistDetails['color'];
    let image_url = wishlistDetails['image_url'];
    let rate = wishlistDetails['rate'];
    let id = [item_id];
    let currency_symbol = wishlistDetails['currency_symbol'];
    let cartData = [currency_symbol, item_id, product_id, productName, productDescription, old_price, price, categoryID, categoryName, size, color, image_url, rate];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('INSERT INTO wishList (currency_symbol,item_id,product_id,productName,productDescription,old_price,price,categoryID,categoryName,siz_e,color,image_url,rate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', cartData)
          .then(() => console.log('Executed wishlist SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  wishListOrNot(wishlistDetails) {
    let product_id = wishlistDetails['product_id'];
    let wishid = [product_id];
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("SELECT * FROM  wishList where product_id ='" + wishid + "'", []).then(results => {
          if (results.rows.length > '0') {
            let id = [results.rows.item(0).id];
            this.deleteFromWishlist(id);
          } else {
            this.addtoWishlist(wishlistDetails);
          }
          this.countWishListLabelTabs();
        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }

  deleteFromWishlist(id) {


    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('DELETE FROM wishList WHERE id = ?', id)
          .then(() => console.log('Executed wish list SQL delete' + id))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }


  createWishList() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('CREATE TABLE IF NOT EXISTS wishList (id INTEGER PRIMARY KEY AUTOINCREMENT,currency_symbol TEXT, item_id INTEGER, product_id INTEGER, productName TEXT, productDescription TEXT, old_price TEXT, price TEXT, categoryID INTEGER, categoryName TEXT, siz_e TEXT, color TEXT, image_url TEXT, rate TEXT)', {})
          .then(() => console.log('Executed SQL create'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  countWishListLabelTabs() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('select count(*) as cnt from wishList', [])
          .then(data => {

            this.events.publish('countWishList', data.rows.item(0).cnt);


          })
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  /**
   * Search History
   */

  createSearchHistory() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('CREATE TABLE IF NOT EXISTS search (id INTEGER PRIMARY KEY AUTOINCREMENT,currency_symbol TEXT, product_id INTEGER, productName TEXT, productDescription TEXT, old_price TEXT, price TEXT, categoryID INTEGER, categoryName TEXT, siz_e TEXT, color TEXT, image_url TEXT, rate TEXT)', {})
          .then(() => console.log('Executed SQL create'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  loadSearchData(): Promise<any[]> {
    let cart: any = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from search ORDER BY id DESC', [])
          .then(data => {


            if (data.rows.length > 0) {

              cart.length = 0;
              for (var i = 0; i < data.rows.length; i++) {
                cart.push({
                  productId: data.rows.item(i).product_id,
                  name: data.rows.item(i).productName,
                  productDescription: data.rows.item(i).productDescription,
                  categoryId: data.rows.item(i).categoryId,
                  categoryName: data.rows.item(i).categoryName,
                  color: data.rows.item(i).color,
                  siz_e: data.rows.item(i).siz_e,
                  image_url: data.rows.item(i).image_url,
                  price: data.rows.item(i).price,
                  old_price: data.rows.item(i).old_price,
                  id: data.rows.item(i).id,
                  rate: data.rows.item(i).rate,
                  currency_symbol: data.rows.item(i).currency_symbol,
                });

              }
            }
            return cart;
          })
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  addToSearch(searchDetails) {
    let product_id = searchDetails['product_id'];
    let productName = searchDetails['productName'];
    let productDescription = searchDetails['productDescription'];
    let old_price = searchDetails['old_price'];
    let price = searchDetails['price'];
    let categoryID = searchDetails['categoryID'];
    let categoryName = searchDetails['categoryName'];
    let size = searchDetails['size'];
    let color = searchDetails['color'];
    let image_url = searchDetails['image_url'];
    let rate = searchDetails['rate'];
    let currency_symbol = searchDetails['currency_symbol'];
    let cartData = [currency_symbol, product_id, productName, productDescription, old_price, price, categoryID, categoryName, size, color, image_url, rate];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('INSERT INTO search (currency_symbol,product_id,productName,productDescription,old_price,price,categoryID,categoryName,siz_e,color,image_url,rate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', cartData)
          .then(() => console.log('Executed search SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  searchOrNot(searchDetails) {
    let product_id = searchDetails['product_id'];
    let searchid = [product_id];
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("SELECT * FROM  search where product_id ='" + searchid + "'", []).then(results => {
          if (results.rows.length > '0') {
            // let id = [results.rows.item(0).id];
            // //this.deleteFromWishlist(id);
          } else {
            this.addToSearch(searchDetails);
          }
        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }

  deleteSearch() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("DELETE FROM search", []).then(results => {

        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }

  /**
   * Shipping Address
   */

  createShippingAddress() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('CREATE TABLE IF NOT EXISTS shipping_address (id INTEGER PRIMARY KEY AUTOINCREMENT, address_id INTEGER, first_name TEXT, last_name TEXT,  phone_number TEXT, email TEXT, country TEXT, state TEXT, town TEXT, address TEXT)', {})
          .then(() => console.log('Executed SQL create'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }


  loadShippingAddress(): Promise<any[]> {
    let shipping: any = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from shipping_address ORDER BY id DESC', [])
          .then(data => {


            if (data.rows.length > 0) {

              shipping.length = 0;
              for (var i = 0; i < data.rows.length; i++) {
                shipping.push({
                  address_id: data.rows.item(i).address_id,
                  first_name: data.rows.item(i).first_name,
                  last_name: data.rows.item(i).last_name,
                  phone_number: data.rows.item(i).phone_number,
                  country: data.rows.item(i).country,
                  state: data.rows.item(i).state,
                  town: data.rows.item(i).town,
                  address: data.rows.item(i).address,
                  email: data.rows.item(i).email,
                });

              }
            }

            return shipping;
          })
          .catch(e => console.log(e));

      })
      .catch(e => console.log(e));

  }

  addShipping(searchDetails) {
    this.deleteShipping();
    let address_id = searchDetails['address_id'];
    let first_name = searchDetails['first_name'];
    let last_name = searchDetails['last_name'];
    let phone_number = searchDetails['phone_number'];
    let country = searchDetails['country'];
    let state = searchDetails['state'];
    let town = searchDetails['town'];
    let address = searchDetails['address'];
    let email = searchDetails['email'];
    let shippingData = [address_id, first_name, last_name, phone_number, country, state, town, address, email];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('INSERT INTO shipping_address (address_id,first_name,last_name,phone_number,country,state,town,address,email) VALUES (?,?,?,?,?,?,?,?,?)', shippingData)
          .then(() => console.log('Executed search SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  deleteShipping() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("DELETE FROM shipping_address", []).then(results => {

        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }

  /**
   * Shipping Address
   */

  createBillingAddress() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('CREATE TABLE IF NOT EXISTS billing_address (id INTEGER PRIMARY KEY AUTOINCREMENT, address_id INTEGER, first_name TEXT, last_name TEXT,  phone_number TEXT, email TEXT, country TEXT, state TEXT, town TEXT, address TEXT)', {})
          .then(() => console.log('Executed SQL create'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }


  loadBillingAddress(): Promise<any[]> {
    let billing: any = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from billing_address ORDER BY id DESC', [])
          .then(data => {


            if (data.rows.length > 0) {

              billing.length = 0;
              for (var i = 0; i < data.rows.length; i++) {
                billing.push({
                  address_id: data.rows.item(i).address_id,
                  first_name: data.rows.item(i).first_name,
                  last_name: data.rows.item(i).last_name,
                  phone_number: data.rows.item(i).phone_number,
                  country: data.rows.item(i).country,
                  state: data.rows.item(i).state,
                  town: data.rows.item(i).town,
                  address: data.rows.item(i).address,
                  email: data.rows.item(i).email,
                });


              }
            }

            return billing;
          })
          .catch(e => console.log(e));

      })
      .catch(e => console.log(e));

  }

  addBilling(searchDetails) {
    this.deleteBilling();
    let address_id = searchDetails['address_id'];
    let first_name = searchDetails['first_name'];
    let last_name = searchDetails['last_name'];
    let phone_number = searchDetails['phone_number'];
    let country = searchDetails['country'];
    let state = searchDetails['state'];
    let town = searchDetails['town'];
    let address = searchDetails['address'];
    let email = searchDetails['email'];
    let shippingData = [address_id, first_name, last_name, phone_number, country, state, town, address, email];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('INSERT INTO billing_address (address_id,first_name,last_name,phone_number,country,state,town,address,email) VALUES (?,?,?,?,?,?,?,?,?)', shippingData)
          .then(() => console.log('Executed search SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  deleteBilling() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("DELETE FROM billing_address", []).then(results => {

        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }

  /**
   * Profile
   */

  createProfile() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        db.executeSql('CREATE TABLE IF NOT EXISTS profile (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, display_name TEXT, image TEXT, uid TEXT)', {})
          .then(() => console.log('Executed SQL create'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }


  loadProfile(): Promise<any[]> {
    let shipping: any = [];
    return this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {


        return db.executeSql('select * from profile ORDER BY id DESC', [])
          .then(data => {


            if (data.rows.length > 0) {

              shipping.length = 0;
              for (var i = 0; i < data.rows.length; i++) {
                shipping.push({
                  email: data.rows.item(i).email,
                  display_name: data.rows.item(i).display_name,
                  image: data.rows.item(i).image,
                  uid: data.rows.item(i).uid,
                });

              }
            }

            return shipping;
          })
          .catch(e => console.log(e));

      })
      .catch(e => console.log(e));

  }

  addProfile(profile) {
    this.deleteProfile();
    let email = profile['email'];
    let display_name = profile['display_name'];
    let image = profile['image'];
    let uid = profile['uid'];
    let profileData = [email, display_name, image, uid];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('INSERT INTO profile (email,display_name,image,uid) VALUES (?,?,?,?)', profileData)
          .then(() => console.log('Executed search SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  updateProfile(profile) {
    this.deleteProfile();
    let email = profile['email'];
    let display_name = profile['display_name'];
    let image = profile['image'];
    let uid = profile['uid'];
    let profileData = [email, display_name, image, uid];

    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.deleteProfile();
        db.executeSql('UPDATE profile SET email = ?, display_name = ?, image = ? WHERE uid = ?', profileData)
          .then(() => console.log('Executed search SQL insert'))
          .catch(e => console.log(e));


      })
      .catch(e => console.log(e));
  }

  deleteProfile() {
    this.sqlite.create({
      name: 'shopping.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql("DELETE FROM profile", []).then(results => {

        })
          .catch(err => console.log(err));


      })
      .catch(e => console.log(e));
  }


}
