import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  public post: any[];
  public api_url: string = 'http://shopaholic.allthingzcode.com/api/sm/';

  constructor(public http: Http) {
    console.log('Hello DataProvider Provider');
  }

  getSlider(): Observable<any[]> {
    return this.http.get(this.api_url + 'slider')
      .map(res => {
        let body = res.json();
        console.log(body);
        return body || {};
      })
      .catch(this.handleError);

  }

  getSettings(): Observable<any[]> {
    return this.http.get(this.api_url + 'get-settings')
      .map(res => {
        let body = res.json();
        console.log(body);
        return body || {};
      })
      .catch(this.handleError);

  }


  getProductsAndCategriesHome(): Observable<any[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');

    return this.http.get(this.api_url + 'getCategoryProducts', {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);

  }

  getCategoriesAll(): Observable<any[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept', 'application/json');
    headers.append('content-type', 'application/json');

    return this.http.get(this.api_url + 'getCategory', {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);

  }

  getProductsByCategory(id): Observable<any[]> {

    let body = {
      category_id: id
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'getProductsByCategory', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);

  }

  getProductsImages(id): Observable<any[]> {

    let body = {
      product_id: id
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'getProductImages', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);

  }

  getProfile(id): Observable<any[]> {

    let body = {
      firebase_id: id
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'get-profile', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);

  }

  search(name): Observable<any[]> {
    let body = {
      search_term: name
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'search-products', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  saveUser(email, uid): Observable<any[]> {
    let body = {
      email: email,
      firebase_id: uid
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'saveProfile', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  getShippingBillingAdress(uid): Observable<any[]> {

    let body = {
      firebase_id: uid
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'getShipingBilingAddress', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  getAddress(uid): Observable<any[]> {

    let body = {
      firebase_id: uid
    };

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'getAddress', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  saveShippingAddress(shippingData): Observable<any[]> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'addShippingAddress', JSON.stringify(shippingData), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  saveBillingAddress(BillingData): Observable<any[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'saveBillingAdress', JSON.stringify(BillingData), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  stripePayment(cardData): Observable<any[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'stripe-payment', JSON.stringify(cardData), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  makePayment(cardData): Observable<any[]> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'make-payment', JSON.stringify(cardData), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  getOrders(uid): Observable<any[]> {
    let body = {
      firebase_id: uid
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'orderhistory', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  getOrderItems(order_id): Observable<any[]> {
    let body = {
      order_id: order_id
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api_url + 'get-Order-Items', JSON.stringify(body), {headers: headers})
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  getCountries(): Observable<any[]> {

    return this.http.get('assets/country.json')
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }

  getMonths(): Observable<any[]> {

    return this.http.get('assets/month.json')
      .map(res => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError);
  }


  private handleError(error: any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return Observable.throw(errMsg);
  }


}
